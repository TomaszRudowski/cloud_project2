package main

import "strconv"

/* Discord payload requirements...
{"content":"some content",
	"embeds":[
		{"fields":[
			{"name":"baseCurrency", "value":"EUR"},
			{"name":"targetCurrency", "value":"NOK"},
			{"name":"currentRate", "value":"9.38"},
			{"name":"minTriggerValue", "value":"1.50"},
			{"name":"maxTriggerValue", "value":"2.55"}
		]}
	]

}
 */
/**
 * necessary to create correct payload accepted by Discord webhooks
 */
type DiscordPayload struct {
	Content string			`json:"content"`
	Embeds [1]DiscordFields	`json:"embeds"`
}

/**
 * necessary to create correct payload accepted by Discord webhooks
 */
type DiscordFields struct {
	Fields [] DiscordField	`json:"fields"`
}

/**
 * necessary to create correct payload accepted by Discord webhooks
 */
type DiscordField struct {
	Name string				`json:"name"`
	Value string			`json:"value"`
}

func formatDiscordPayload (webhook Webhook, currentRate float32) DiscordPayload {
	rateAsString := strconv.FormatFloat(float64(currentRate), 'f', 2, 32)
	minAsString := strconv.FormatFloat(float64(webhook.MinTriggerValue), 'f', 2, 32)
	maxAsString := strconv.FormatFloat(float64(webhook.MaxTriggerValue), 'f', 2, 32)

	var discordPayload DiscordPayload
	discordPayload.Content = "Exchange rate update from webhook " + webhook.ID
	var fields DiscordFields
	fields.Fields = append(fields.Fields, DiscordField{"baseCurrency", webhook.BaseCurrency})
	fields.Fields = append(fields.Fields, DiscordField{"targetCurrency", webhook.TargetCurrency})
	fields.Fields = append(fields.Fields, DiscordField{"currentRate", rateAsString})
	fields.Fields = append(fields.Fields, DiscordField{"minTriggerValue", minAsString})
	fields.Fields = append(fields.Fields, DiscordField{"maxTriggerValue", maxAsString})

	discordPayload.Embeds[0] = fields

	return discordPayload
}