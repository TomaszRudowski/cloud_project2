package main

import (
	"net/http"
	"fmt"
	"io/ioutil"
	"encoding/json"
	"strings"
	"strconv"
	"bytes"
	"log"
)

type Rates struct {
	Base string 					`json:"base"`
	Date string 					`json:"date"`
	RatesList map[string]float32  	`json:"rates"`
}

type Webhook struct {
	ID 				string
	WebhookURL 		string	`json:"webhookURL"`
	BaseCurrency 	string	`json:"baseCurrency"`
	TargetCurrency 	string	`json:"targetCurrency"`
	MinTriggerValue float32	`json:"minTriggerValue"`
	MaxTriggerValue float32	`json:"maxTriggerValue"`
}

type SubscribePayload struct {
	WebhookURL 		string	`json:"webhookURL"`
	BaseCurrency 	string	`json:"baseCurrency"`
	TargetCurrency 	string	`json:"targetCurrency"`
	MinTriggerValue float32	`json:"minTriggerValue"`
	MaxTriggerValue float32	`json:"maxTriggerValue"`
}

type InvokePayload struct {
	BaseCurrency 	string 	`json:"baseCurrency"`
	TargetCurrency 	string 	`json:"targetCurrency"`
	CurrentRate		float32	`json:"currentRate"`
	MinTriggerValue float32	`json:"minTriggerValue"`
	MaxTriggerValue float32	`json:"maxTriggerValue"`
}


var localWebhooks [] Webhook	// temp used instead of database

//var dummyRequest = `{"base":"EUR","date":"2017-10-13","rates":{"AUD":1.508,"BGN":1.9558,"BRL":3.7517,"CAD":1.4761,"CHF":1.1533,"CNY":7.7831,"CZK":25.813,"DKK":7.444,"GBP":0.8898,"HKD":9.221,"HRK":7.5085,"HUF":308.64,"IDR":15939.0,"ILS":4.1328,"INR":76.712,"JPY":132.49,"KRW":1333.9,"MXN":22.388,"MYR":4.9832,"NOK":9.339,"NZD":1.6541,"PHP":60.731,"PLN":4.2555,"RON":4.5865,"RUB":68.12,"SEK":9.605,"SGD":1.6013,"THB":39.115,"TRY":4.3225,"USD":1.181,"ZAR":15.805}}`

var dummyCounter = 1		// temp to create fake Id for webhook

var ww http.ResponseWriter	// temp to get local output

func handleWebhook (w http.ResponseWriter, r *http.Request) {

	fmt.Fprintln(w, "handleWebhook")
}

func handleSubscribe (w http.ResponseWriter, r *http.Request) {

	var newWebhook Webhook
	errorMsg := "Cannot subscribe new webhook. Use POST with proper payload."

	method := r.Method
	if strings.Compare(method, "POST") == 0 {
		JSONdata, err := ioutil.ReadAll(r.Body)
		r.Body.Close()
		if err != nil {
			http.Error(w, errorMsg, 400)
		}
		json.Unmarshal(JSONdata, &newWebhook)
		newWebhook.ID = createUniqueId()
		subscribeNewWebhook(newWebhook)
		fmt.Fprintf(w, "Subscribing succesful. Your webhook Id: %s\nNote it for future refferance!\n", newWebhook.ID)
	} else  {
		http.Error(w, errorMsg, 400)
	}
}

func handleLatestRate (w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "handleLatestRate")
}

func handleAverageRate (w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "handleAverageRate")
}

func handleUpdateAllRateChanges (w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintln(w, "handleInvokeAll")
	tempWebhooks := getAllWebhooks()

	ww = w		// temp

	allSucceeded := true

	for _, webhook := range tempWebhooks {
		currentRate := getCurrentRates(webhook.BaseCurrency, webhook.TargetCurrency)
		if currentRate > 0 {
			if !testInvoking (webhook, currentRate) {
				allSucceeded = false
			}
		} else {
			// 0.0 if error during getting data from ticker
			//fmt.Fprintf(w, "Webhook %s - error during receiving current data\n", webhook.Id)
		}
	}

	if !allSucceeded {
		http.Error(w, "Notification was neccessary, but couldn't reach all webhooks", 400)
	}

}

func handleWrongURL (w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Proper use of application [url]/exchange to subscribe new webhook")
}

func subscribeNewWebhook (webhook Webhook) {
	// temporary use of local array instead of database
	// to do: insert into database and get unique id string from there

	localWebhooks = append(localWebhooks, webhook)
}

func createUniqueId () string {
	// temp, create real unique string
	dummyCounter = dummyCounter + 1
	return "dummyId_" + strconv.Itoa(dummyCounter)
}

func getAllWebhooks () [] Webhook {
	// temporary use of local array instead of database
	return localWebhooks
}

func getCurrentRates (baseCurrency string, targetCurrency string) float32 {
	reqURL := "http://api.fixer.io/latest?base=" + baseCurrency

	var tempRates Rates
	// based on https://golang.org/pkg/net/http/#Response
	res, err := http.Get(reqURL)
	if err != nil {
		log.Fatal(err)
		return float32(0.0)	//
	}
	JSONdata, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
		return float32(0.0)	//
	}

	json.Unmarshal(JSONdata, &tempRates)
	return tempRates.RatesList[targetCurrency]
}

func testInvoking (webhook Webhook, currentRate float32) bool {
	succesfulInvoke := true
	// here currentRate is a positive number, 0.0 case taken care of before call
	if webhook.MinTriggerValue > currentRate || webhook.MaxTriggerValue < currentRate {
		succesfulInvoke = publishChange(webhook, currentRate)
	} else {
		fmt.Printf("Webhook %s, no need for invoking, threshold: %f-%f, current rate %f\n",
			webhook.ID, webhook.MinTriggerValue, webhook.MaxTriggerValue, currentRate)
	}
	return succesfulInvoke
}

func publishChange (webhook Webhook, currentRate float32) bool {
	//succesfulPublish := true
	// temporary print
	fmt.Printf("Webhook %s need to be warned at %s about rate change\n",
		webhook.ID, webhook.WebhookURL)
	fmt.Printf("\tObserved currencies: %s %s. Current rate: %f\n",
		webhook.BaseCurrency, webhook.TargetCurrency, currentRate)

	url := webhook.WebhookURL

	var jsonStr []byte
	// optional: method to build correct struct for Discord Webhook
	// jsonStr,_ = json.Marshal(formatDiscordPayload(webhook,currentRate))
	// struct as in assignment text
	jsonStr,_ = json.Marshal(formatStandardInvokePayload(webhook,currentRate))

// based on https://stackoverflow.com/questions/24455147/how-do-i-send-a-json-string-in-a-post-request-in-go
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		//panic(err)	// cannot reach webhook
		return false
	}
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		//panic(err)
		return false
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)		// log
	fmt.Println("response Headers:", resp.Header)		// log
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))		// log
// end of code from stackoverflow

	if resp.StatusCode == http.StatusBadRequest {		// case when all communication is ok
		return false									// but not correct payload format
	}

	ww.Header().Set("Content-Type", "application/json")	// local
	fmt.Fprintf(ww, "%s", jsonStr)

	return true
}

func formatStandardInvokePayload (webhook Webhook, currentRate float32) InvokePayload {
	var payload InvokePayload
	payload.BaseCurrency = webhook.BaseCurrency
	payload.TargetCurrency = webhook.TargetCurrency
	payload.CurrentRate = currentRate
	payload.MinTriggerValue = webhook.MinTriggerValue
	payload.MaxTriggerValue = webhook.MaxTriggerValue

	return payload
}

func main() {

	http.HandleFunc("/exchange/", handleWebhook)		// /root/{id} GET, DELETE
	http.HandleFunc("/exchange", handleSubscribe)	// /root subscribe
	http.HandleFunc("/exchange/latest/", handleLatestRate)
	http.HandleFunc("/exchange/average/", handleAverageRate)
	http.HandleFunc("/exchange/evaluationtrigger/", handleUpdateAllRateChanges)
	http.HandleFunc("/", handleWrongURL)				// without /exchange
	http.ListenAndServe("127.0.0.1:8080", nil)

}
