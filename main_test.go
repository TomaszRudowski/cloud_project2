package main

import (
	"testing"
	"net/http/httptest"
	"net/http"
	"fmt"
	"encoding/json"
	"bytes"
	"io/ioutil"
	"strings"

)

func TestSubscribe (t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(handleSubscribe))
	defer ts.Close()

	webhook := Webhook{"test_id", "http://test_url", "EUR", "NOK", 5.0, 5.5 }

	subscribePayload := SubscribePayload {
		"http://test_url",
		"EUR",
		"NOK",
		5.0,
		5.5 }

	jsonStr,err := json.Marshal(subscribePayload)
	if err != nil {
		t.Errorf("Error durin Marshal call: %s", err)
	}

	ts.URL = ts.URL + "/exchange"

// case 1: correct POST request
	req,err := http.NewRequest("POST",ts.URL,bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Errorf("Error making the POST request: %s", err)
		return
	}
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Errorf( "Error: %s", err)
		return
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)		// log
	fmt.Println("response Headers:", resp.Header)		// log
	bodyPost, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(bodyPost))		// log

// continue case 1, check if webhook stored properly (testing against local array)
	if localWebhooks[0].BaseCurrency != webhook.BaseCurrency ||
			localWebhooks[0].TargetCurrency != webhook.TargetCurrency ||
			localWebhooks[0].MinTriggerValue != webhook.MinTriggerValue ||
			localWebhooks[0].MaxTriggerValue != webhook.MaxTriggerValue	{
		t.Errorf("Error while compering payload values with registered webhook")
		return
	}

// case 2: inncorect GET method
	respGet,err := http.Get(ts.URL)
	if err != nil {
		t.Errorf("Error making the GET subscribe request, %s", err)
		return
	}

	if respGet.StatusCode != http.StatusBadRequest {
		t.Errorf("For route: %s, expected StatusCode %d, received %d", ts.URL,
			http.StatusBadRequest, respGet.StatusCode)
		return
	}
}

func TestInvokeAll (t *testing.T) {
	webhook := Webhook{"test_id", "default", "EUR",
		"NOK", 5.0, 5.5 }

	testWebhookServer := httptest.NewServer(http.HandlerFunc(handleInvoveTest))
	webhook.WebhookURL = testWebhookServer.URL

	localWebhooks[0] = webhook	// replacing webhook from previous test

	ts := httptest.NewServer(http.HandlerFunc(handleUpdateAllRateChanges))
	defer ts.Close()

	ts.URL = ts.URL + "/exchange/evaluationtrigger/"

	respGet,err := http.Get(ts.URL)
	if err != nil {
		t.Errorf("Error making the GET subscribe request, %s", err)
		return
	}

	if respGet.StatusCode == http.StatusBadRequest {
		t.Errorf("For route: %s, not expected StatusCode %d", ts.URL, http.StatusBadRequest)
		return
	}
}

/**
 * help function to handle invoke webhook on a test server
 */
func handleInvoveTest (w http.ResponseWriter, r *http.Request) {
	expectedPayload := InvokePayload{"EUR",
		"NOK", 0, 5.0, 5.5 }

	var updatePayload InvokePayload

	method := r.Method
	if strings.Compare(method, "POST") == 0 {
		JSONdata, err := ioutil.ReadAll(r.Body)
		r.Body.Close()
		if err != nil {
			http.Error(w, "Error reading request body", 400)
		}
		json.Unmarshal(JSONdata, &updatePayload)
		// when here it is succesfully notified, checkig received payload with pattern
		// to avoid double requst to ticker, assume that currentRate is outside 5-5.5 and test it manually

		if updatePayload.BaseCurrency != expectedPayload.BaseCurrency ||
			updatePayload.TargetCurrency != expectedPayload.TargetCurrency ||
			updatePayload.MinTriggerValue != expectedPayload.MinTriggerValue ||
			updatePayload.MaxTriggerValue != expectedPayload.MaxTriggerValue ||
			(updatePayload.CurrentRate > updatePayload.MinTriggerValue &&
				updatePayload.CurrentRate < updatePayload.MaxTriggerValue) {

			http.Error(w,"Not correct payload format", 400)
		}

	} else  {
		http.Error(w, "Not correct method, use POST to notify webhook", 400)
	}
}

